<?php

namespace vr\sms;

use yii\base\Component;

/**
 * Class SmsMessage
 * @package vr\sms
 */
class SmsMessage extends Component
{
    /**
     * @var SmsProvider
     */
    public $provider;

    /**
     * @var
     */
    public $recipient;

    /**
     * @var
     */
    public $text;

    /**
     * @var
     */
    public $errorException;

    /**
     * @param $phone
     *
     * @return $this
     */
    public function setTo($phone)
    {
        $this->recipient = $phone;

        return $this;
    }

    /**
     * @param $text
     *
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function send()
    {
        return $this->provider->deliver($this);
    }

    /**
     * @return mixed
     */
    public function verifyPhone()
    {
        return $this->provider->verifyPhone($this->recipient);
    }
}