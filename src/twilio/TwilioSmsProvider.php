<?php
namespace vr\sms\twilio;

use vr\sms\SmsMessage;

/**
 * Class TwilioSmsProvider
 * @package vr\sms\twilio
 */
class TwilioSmsProvider extends \vr\sms\SmsProvider
{
    /**
     * @var
     */
    public $sid;

    /**
     * @var
     */
    public $authToken;

    /**
     * @var
     */
    public $sender;

    /**
     * @param SmsMessage $message
     *
     * @return mixed
     */
    public function deliver(SmsMessage $message)
    {
        $http = new \Services_Twilio_TinyHttp(
            'https://api.twilio.com',
            [
                'curlopts' => [
                    CURLOPT_SSL_VERIFYPEER => true,
                    CURLOPT_SSL_VERIFYHOST => 2,
                ]
            ]
        );

        $client = new \Services_Twilio($this->sid, $this->authToken, "2010-04-01", $http);

        return $client->account->messages->sendMessage($this->sender, $message->recipient, $message->text);
    }

    /**
     * @param $number
     *
     * @return bool|mixed
     */
    public function verifyPhone($number)
    {
        $client = new \Lookups_Services_Twilio(
            \Yii::$app->params['twillioSid'],
            \Yii::$app->params['twillioAuthToken']
        );

        $number = $client->phone_numbers->get($number);

        try {
            if ($number->phone_number) {
                return true;
            }

            return false;
        } catch (\Services_Twilio_RestException $e) {
            return false;
        }
    }
}