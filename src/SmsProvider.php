<?php
namespace vr\sms;

/**
 * Class SmsProvider
 * @package vr\sms
 */
abstract class SmsProvider
{
    /**
     * @return SmsMessage
     */
    public function compose()
    {
        return new SmsMessage([
            'provider' => $this
        ]);
    }

    /**
     * @param SmsMessage $message
     *
     * @return mixed
     */
    public abstract function deliver(SmsMessage $message);

    /**
     * @param $number
     *
     * @return mixed
     */
    public abstract function verifyPhone($number);
}